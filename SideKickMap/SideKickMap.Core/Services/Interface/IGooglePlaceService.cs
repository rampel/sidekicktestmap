﻿using System;
using System.Threading.Tasks;
using SideKickMap.Core.Models;

namespace SideKickMap.Core.Services.Interface
{
    public interface IGooglePlaceService
    {
        Task<Response<GoogleDTO>> GetPlaces(GetGooglePlaceCommand command);

    }
}
