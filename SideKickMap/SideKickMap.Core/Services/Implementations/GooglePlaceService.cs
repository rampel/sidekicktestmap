﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SideKickMap.Core.Models;
using SideKickMap.Core.Resources;
using SideKickMap.Core.Services.Interface;

namespace SideKickMap.Core.Services.Implementations
{
    public class GooglePlaceService : BaseWebService, IGooglePlaceService
    {
        public GooglePlaceService(IRestService restService) : base(restService)
        {
        }

        public async Task<Response<GoogleDTO>> GetPlaces(GetGooglePlaceCommand command)
        {
            Response<GoogleDTO> responseData = new Response<GoogleDTO>();
            try
            {
                var uri = new Uri(string.Format(restService.CreateGetURL("https://maps.googleapis.com/maps/api/place/textsearch/json?", command)));

                var request = new HttpRequestMessage(HttpMethod.Get, uri);
                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);
                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    var jo = JObject.Parse(stringContent);
                    if (jo != null)
                    {
                        var parsedResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<GoogleDTO>(jo.ToString());
                        if (parsedResponse != null)
                        {
                            responseData.Data = parsedResponse;
                            responseData.Success = true;
                        }
                        else
                        {
                            responseData.ErrorMessage = jo["error_message"]?.ToString();
                            responseData.Success = false;
                        }
                    }
                    System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");
                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"{HttpMethod.Get.ToString()}:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception)
            {
                responseData.ErrorMessage = Errors.UnableToConnectToGoogle;
            }
            return responseData;
        }
    }
}
