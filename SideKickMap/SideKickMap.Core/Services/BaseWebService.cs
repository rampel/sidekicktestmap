﻿using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SideKickMap.Core.Services.Interface;
using SideKickMap.Core.Resources;

namespace SideKickMap.Core.Services
{
    public abstract class BaseWebService
    {
        public const int DefaultBufferSize = 8192;
        public HttpClient client;
        public IRestService restService;

        public BaseWebService(IRestService restService)
        {
            this.restService = restService;
            this.client = restService.client;
        }

        public async Task<string> HandleErrorResponse(HttpResponseMessage response)
        {
            try
            {
                /**/
                if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    if (!string.IsNullOrWhiteSpace(stringContent))
                    {
                        var jo = JObject.Parse(stringContent);
                        var error = jo["error_message"]?.ToString();
                        if (!string.IsNullOrEmpty(error))
                        {
                            return error;
                        }
                        else
                        {
                            return Errors.UnableToConnect;
                        }
                    }
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    if (!string.IsNullOrWhiteSpace(stringContent))
                    {
                        var jo = JObject.Parse(stringContent);
                        var error = jo["error_message"]?.ToString();
                        if (!string.IsNullOrEmpty(error))
                        {
                            return error;
                        }
                        else
                        {
                            return Errors.UnableToConnect;
                        }
                    }
                }
                else
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    if (!string.IsNullOrWhiteSpace(stringContent))
                    {
                        var jo = JObject.Parse(stringContent);
                        var error = jo["error_message"]?.ToString();
                        if (!string.IsNullOrEmpty(error))
                        {
                            return error;
                        }
                        else
                        {
                            return Errors.UnableToConnect;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return $"{Errors.UnableToConnect} {ex.Message}";
            }
            return Errors.UnableToConnect;
        }
    }
}

