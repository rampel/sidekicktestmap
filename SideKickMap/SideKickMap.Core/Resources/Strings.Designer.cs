﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SideKickMap.Core.Resources {
    using System;
    using System.Reflection;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Strings {
        
        private static System.Resources.ResourceManager resourceMan;
        
        private static System.Globalization.CultureInfo resourceCulture;
        
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Strings() {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static System.Resources.ResourceManager ResourceManager {
            get {
                if (object.Equals(null, resourceMan)) {
                    System.Resources.ResourceManager temp = new System.Resources.ResourceManager("SideKickMap.Core.Resources.Strings", typeof(Strings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        internal static string YouHaventAddYourFavoriteAddress {
            get {
                return ResourceManager.GetString("YouHaventAddYourFavoriteAddress", resourceCulture);
            }
        }
        
        internal static string ShowFavoriteAddresses {
            get {
                return ResourceManager.GetString("ShowFavoriteAddresses", resourceCulture);
            }
        }
        
        internal static string Add {
            get {
                return ResourceManager.GetString("Add", resourceCulture);
            }
        }
        
        internal static string AddAddress {
            get {
                return ResourceManager.GetString("AddAddress", resourceCulture);
            }
        }
        
        internal static string SearchAddress {
            get {
                return ResourceManager.GetString("SearchAddress", resourceCulture);
            }
        }
        
        internal static string NoResultsFound {
            get {
                return ResourceManager.GetString("NoResultsFound", resourceCulture);
            }
        }
    }
}
