﻿using System;
using Newtonsoft.Json;

namespace SideKickMap.Core
{
    public class GetGooglePlaceCommand
    {
        public string Query { get; set; }
        public string Fields { get; set; }
        public string Key { get; set; }
    }
}
