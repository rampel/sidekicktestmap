﻿using MvvmCross.ViewModels;
using SideKickMap.Core.Resources;

namespace SideKickMap.Core.ViewModels
{
    public abstract class BaseViewModel : MvxViewModel
    {
        protected BaseViewModel()
        {       
        }
        public string this[string index] => Strings.ResourceManager.GetString(index);
    }

    public abstract class BaseViewModel<TParameter, TResult> : MvxViewModel<TParameter, TResult>
        where TParameter : class
        where TResult : class
    {
        protected BaseViewModel()
        {
        }
       
        public string this[string index] => Strings.ResourceManager.GetString(index);
    }
}
