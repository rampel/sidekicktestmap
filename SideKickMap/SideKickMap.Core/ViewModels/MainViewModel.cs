﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using SideKickMap.Core.Models;
using SideKickMap.Core.Repository.Interface;
using SideKickMap.Core.Resources;

namespace SideKickMap.Core.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private readonly IMvxNavigationService _navigationService;
        private readonly IAddressRepository _addressRepository;
        public event EventHandler<Address> ShowMapLocation;

        private string _favoriteAddressText;
        public string FavoriteAddressText
        {
            get
            {
                return _favoriteAddressText;
            }
            set
            {
                _favoriteAddressText = value;
                RaisePropertyChanged(() => FavoriteAddressText);
            }
        }

        private string _addAddressText;
        public string AddAddressText
        {
            get
            {
                return _addAddressText;
            }
            set
            {
                _addAddressText = value;
                RaisePropertyChanged(() => AddAddressText);
            }
        }
        

        private MvxObservableCollection<Address> _favoriteAddresses;
        public MvxObservableCollection<Address> FavoriteAddresses
        {
            get
            {
                return _favoriteAddresses;
            }
            set
            {
                _favoriteAddresses = value;
                RaisePropertyChanged(() => FavoriteAddresses);
            }
        }

        /// <summary>
        /// Commands 
        /// </summary>
        public IMvxAsyncCommand SearchAddressCommand { get; private set; }
        public IMvxCommand<Address> AddressSelectedCommand { get; private set; }

        public MainViewModel(IMvxNavigationService navigationService, IAddressRepository addressRepository)
        {
            _navigationService = navigationService;
            _addressRepository = addressRepository;
            FavoriteAddresses = new MvxObservableCollection<Address>();

            SearchAddressCommand = new MvxAsyncCommand(async () =>
            {
                await navigationService.Navigate<AddAddressViewModel>();
            });

            AddressSelectedCommand = new MvxCommand<Address>((e) =>
            {
                ShowMapLocation?.Invoke(this, e);
            });
        }

        public void OnMapReady() {
            if (FavoriteAddresses?.Count > 0) {
                ShowMapLocation?.Invoke(this, FavoriteAddresses[0]);
            }
        }

        public override void ViewAppeared()
        {
            base.ViewAppeared();
            LoadFavoriteAddress();
        }

        public async Task LoadFavoriteAddress()
        {
            FavoriteAddresses?.Clear();
            var results = await _addressRepository.GetAddressAsync();
            if (results != null)
            {
                FavoriteAddresses.AddRange(results);
            }

            FavoriteAddressText = FavoriteAddresses.Count > 0 ? Strings.ShowFavoriteAddresses : Strings.YouHaventAddYourFavoriteAddress;
            AddAddressText = FavoriteAddresses.Count > 0 ? Strings.Add : Strings.AddAddress;
            if (FavoriteAddresses?.Count > 0)
            {
                await Task.Delay(500);
                ShowMapLocation?.Invoke(this, FavoriteAddresses[0]);
            }
        }
    }
}
