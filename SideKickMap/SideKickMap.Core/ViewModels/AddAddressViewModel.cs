﻿using System;
using System.Threading.Tasks;
using System.Timers;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using SideKickMap.Core.Models;
using SideKickMap.Core.Repository.Interface;
using SideKickMap.Core.Services.Interface;

namespace SideKickMap.Core.ViewModels
{
    public class AddAddressViewModel : BaseViewModel
    {
       
        /// <summary>
        /// Commands
        /// </summary>
        public IMvxAsyncCommand<Address> AddressSelectedCommand { get; private set; }

        private const string FIELDS = "name,geometry";
        private const int TIME_DELAY = 500;
        private Timer delayType;

        private readonly IGooglePlaceService _googlePlaceService;
        private readonly IMvxNavigationService _navigationService;
        private readonly IAddressRepository _addressRepository;

        public AddAddressViewModel(IMvxNavigationService navigationService, IGooglePlaceService googlePlayService, IAddressRepository addressRepository)
        {
            _navigationService = navigationService;
            _googlePlaceService = googlePlayService;
            _addressRepository = addressRepository;

            SearchedAddresses = new MvxObservableCollection<Address>();

            delayType = new Timer(TIME_DELAY);
            delayType.Elapsed += DelayType_Elapsed;

            AddressSelectedCommand = new MvxAsyncCommand<Address>( async (e) =>
            {
                var response = await _addressRepository.SaveAddressAsync(e);
                await _navigationService.Close(this);
            });

            ShowEmptyText = false;
        }

        private async void DelayType_Elapsed(object sender, ElapsedEventArgs e)
        {
            delayType.Stop();
            await SearchPlace();
        }

        async Task SearchPlace() {
            ShowLoader = true;
            ShowEmptyText = false;

            var response = await _googlePlaceService.GetPlaces(new GetGooglePlaceCommand {
               Fields = FIELDS,
               Key = SideKickMap.Core.App.GKEY,
               Query = SearchAddressText.Trim()
            });
            SearchedAddresses.Clear();
            if (response.Success) {
                foreach (var places in response.Data.Results)
                {
                    SearchedAddresses.Add(new Address { Latitude = places.Geometry.Location.Latitude, Longitude = places.Geometry.Location.Longitude, Name = places.Address  });
                }
            }

            ShowEmptyText = SearchedAddresses?.Count <= 0;
            ShowLoader = false;
        }

        private string searchAddressText;
        public string SearchAddressText
        {
            get { return searchAddressText; }
            set
            {
                searchAddressText = value;
                if (!string.IsNullOrEmpty(value))
                {
                    delayType?.Stop();
                    delayType?.Start();
                }
                RaisePropertyChanged(() => SearchAddressText);
            }
        }

        private bool showLoader;
        public bool ShowLoader
        {
            get { return showLoader; }
            set
            {
                showLoader = value;
                RaisePropertyChanged(() => ShowLoader);
            }
        }

        private bool showEmptyText;
        public bool ShowEmptyText
        {
            get { return showEmptyText; }
            set
            {
                showEmptyText = value;
                RaisePropertyChanged(() => ShowEmptyText);
            }
        }

        private MvxObservableCollection<Address> _searchedAddresses;
        public MvxObservableCollection<Address> SearchedAddresses
        {
            get
            {
                return _searchedAddresses;
            }
            set
            {
                _searchedAddresses = value;
                RaisePropertyChanged(() => SearchedAddresses);
            }
        }

    }
}
