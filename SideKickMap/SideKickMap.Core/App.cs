﻿using Acr.UserDialogs;
using AppStandardLibrary;
using MvvmCross;
using MvvmCross.IoC;
using MvvmCross.ViewModels;

namespace SideKickMap.Core
{
    public class App : MvxApplication
    {
        public const string GKEY = "AIzaSyBsOzoyYABTzVEXGKwBlBGd-qO-DX2B7Hc";

        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            CreatableTypes()
                .EndingWith("Client")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            CreatableTypes()
                .EndingWith("Repository")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            Mvx.IoCProvider.RegisterSingleton<IUserDialogs>(() => UserDialogs.Instance);
            // register the appstart object
            RegisterCustomAppStart<AppStart>();
        }
    }
}
