﻿using System;
using SQLite;

namespace SideKickMap.Core.Models
{
    public class Address
    {
       [PrimaryKey, AutoIncrement]  
       public int Id { get; set; }
       public string Name { get; set; }
       public double Latitude { get; set; }
       public double Longitude { get; set; }
       public DateTime DateCreated { get; set; }

    }
}
