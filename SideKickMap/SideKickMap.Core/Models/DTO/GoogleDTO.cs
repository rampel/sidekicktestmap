﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SideKickMap.Core.Models
{
    public class GoogleDTO
    {
        [JsonProperty("results")]
        public List<GooglePlaceData> Results { get; set; }
    }

    public class GooglePlaceData
    {
        [JsonProperty("formatted_address")]
        public string Address { get; set; }

        [JsonProperty("geometry")]
        public GoogleGeometry Geometry { get; set; }
    }

    public class GoogleGeometry
    {
        [JsonProperty("location")]
        public GoogleLocation Location { get; set; }
    }

    public class GoogleLocation
    {
        [JsonProperty("lat")]
        public double Latitude { get; set; }

        [JsonProperty("lng")]
        public double Longitude { get; set; }
    }
}
