﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SideKickMap.Core.Models;

namespace SideKickMap.Core.Repository.Interface
{
    public interface IAddressRepository
    {
        Task<bool> SaveAddressAsync(Address item);

        Task<List<Address>> GetAddressAsync();
    }
}
