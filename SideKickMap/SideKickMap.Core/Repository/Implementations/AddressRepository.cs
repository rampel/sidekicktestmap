﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AppStandardLibrary;
using SideKickMap.Core.Models;
using SideKickMap.Core.Repository.Interface;
using SQLite;
using System.Linq;

namespace SideKickMap.Core.Repository.Implementations
{
    public class AddressRepository : IAddressRepository
    {
        readonly SQLiteAsyncConnection database;

        public AddressRepository(IFileHelper fileHelper)
        {
            database = new SQLiteAsyncConnection(fileHelper.GetLocalFilePath("sidekickmap.db3"));
        }

        public async Task<List<Address>> GetAddressAsync()
        {
            await database.CreateTableAsync<Address>();
            return await database.Table<Address>().OrderByDescending(x => x.DateCreated).ToListAsync();
        }

        public async Task<bool> SaveAddressAsync(Address item)
        {
            var data = await database.Table<Address>().Where(x => x.Name == item.Name).FirstOrDefaultAsync();
            if (data != null) {
                await database.DeleteAsync(data);
            }
            item.DateCreated = DateTime.Now.ToLocalTime();
            return await database.InsertAsync(item) != 0;
        }
    }
}
