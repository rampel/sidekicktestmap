﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using SideKickMap.Core.ViewModels;

namespace SideKickMap.Droid.Views
{
    [MvxActivityPresentation]
    [Activity(Label = "Add Address",
     Theme = "@style/AppTheme",
     Name = "sidekickmap.droid.views.AddAddressView"
     )]
    public class AddAddressView : MvxAppCompatActivity<AddAddressViewModel>
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.add_address_view);

            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
        }

        protected override void OnResume()
        {
            base.OnResume();
            //show keyboard on launch
            var etSearch = FindViewById<EditText>(Resource.Id.etSearch);
            etSearch.RequestFocus();
            InputMethodManager inputMethodManager = GetSystemService(Context.InputMethodService) as InputMethodManager;
            inputMethodManager.ShowSoftInput(etSearch, ShowFlags.Forced);
            inputMethodManager.ToggleSoftInput(ShowFlags.Forced, HideSoftInputFlags.ImplicitOnly);
        }

        protected override void OnPause()
        {
            base.OnPause();
            InputMethodManager inputMethodManager = GetSystemService(Context.InputMethodService) as InputMethodManager;
            inputMethodManager.HideSoftInputFromWindow(this.CurrentFocus.WindowToken, HideSoftInputFlags.None);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}
