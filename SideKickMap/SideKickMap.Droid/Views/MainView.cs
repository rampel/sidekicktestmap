﻿using Acr.UserDialogs;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Locations;
using Android.Support.Design.Widget;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Views.Animations;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using SideKickMap.Core.ViewModels;

namespace SideKickMap.Droid
{
    [MvxActivityPresentation]
    [Activity(Label = "Sidekick Map",
        Theme = "@style/AppTheme",
        LaunchMode = LaunchMode.SingleTop,
        Name = "sidekickmap.droid.views.MainView"
        )]
    public class MainView : MvxAppCompatActivity<MainViewModel>, IOnMapReadyCallback
    {
        private MapView mapView;
        public GoogleMap googleMap;

        protected override void OnCreate(Android.OS.Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.main_view);
            InitializeMapView();
            var recyclerView = FindViewById<MvxRecyclerView>(Resource.Id.rvAddress);
            recyclerView.Adapter = new SelectedAnimatorRecyclerAdapter((IMvxAndroidBindingContext)BindingContext);

        }

        void InitializeMapView() {
            mapView = FindViewById<MapView>(Resource.Id.map);
            mapView.OnCreate(null);
            mapView.GetMapAsync(this);
        }

        public void OnMapReady(GoogleMap googleMap)
        {
            this.googleMap = googleMap;
            ViewModel.OnMapReady();
        }

        protected override void OnResume()
        {
            base.OnResume();
            mapView.OnResume();
            ViewModel.ShowMapLocation += ViewModel_ShowMapLocation; ;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            mapView.OnDestroy();
        }

        protected override void OnStop()
        {
            base.OnStop();
            mapView.OnStop();
        }

        protected override void OnPause()
        {
            base.OnPause();
            mapView.OnPause();
            ViewModel.ShowMapLocation -= ViewModel_ShowMapLocation;
        }

        private void ViewModel_ShowMapLocation(object sender, Core.Models.Address e)
        {
            LatLng location = new LatLng(e.Latitude, e.Longitude);

            CameraPosition.Builder builder = CameraPosition.InvokeBuilder();
            builder.Target(location);
            builder.Zoom(15);
            builder.Tilt(45);

            MarkerOptions markerOpt1 = new MarkerOptions();
            markerOpt1.SetPosition(location);
            markerOpt1.SetTitle(e.Name);

            markerOpt1.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Mipmap.pinmap));

            this.googleMap.AddMarker(markerOpt1).ShowInfoWindow();

            CameraPosition cameraPosition = builder.Build();

            CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);

            this.googleMap.AnimateCamera(cameraUpdate);
        }
    }

    public class SelectedAnimatorRecyclerAdapter : MvxRecyclerAdapter
    {
        public SelectedAnimatorRecyclerAdapter(IMvxAndroidBindingContext bindingContext)
              : base(bindingContext)
        {
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            base.OnBindViewHolder(holder, position);

            holder.ItemView.Click += (s, e) =>
            {
                SetAnimation(holder.ItemView);
            };
        }

        void SetAnimation(View viewToAnimate)
        {
            ScaleAnimation anim = new ScaleAnimation(0.5f, 1.0f, 0.5f, 1.0f, Dimension.RelativeToSelf, 0.5f, Dimension.RelativeToSelf, 0.5f);
            anim.Duration = 400;
            viewToAnimate.StartAnimation(anim);
        }
    }
}

