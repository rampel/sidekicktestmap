﻿using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Widget;
using Com.Airbnb.Lottie;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using MvvmCross.Platforms.Android.Views;

namespace SideKickMap.Droid
{
    [MvxActivityPresentation]
    [Activity(
        MainLauncher = true,
        Icon = "@mipmap/pinmap",
        Theme = "@style/Theme.Splash",
        NoHistory = true,
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashScreen : MvxSplashScreenActivity
    {
        public SplashScreen()
            : base(Resource.Layout.splash_screen)
        {
        
        }

        public async override Task InitializationComplete()
        {
            await PlaySplashAnimation();

            var intent = new Intent(this, typeof(MainView));
            intent.SetData(Intent.Data);
            StartActivity(intent);
        }

        async Task PlaySplashAnimation() {
            var animationView = FindViewById<LottieAnimationView>(Resource.Id.animation_view);
            animationView.Progress = 0f;
            animationView.PlayAnimation();
            await Task.Delay(2000);
        }
    }
}